package br.com.syonet.exercicio01;

public class Cliente {
	
	private String cpf;
	private String nome;
	
	public Cliente(String cpf, String nome) {
		this.cpf = cpf;
		this.nome = nome;
	}
	
	public String toString() {
		return "Cpf: " + cpf + "\n"
				+ "Nome: " + nome + "\n";
	}
	
	public String getCPF() {
		return cpf;
	}
	
}
