package br.com.syonet.exercicio01;

import br.com.syonet.exercicio02.EnumBancos;

public class Conta {

	private Cliente cliente;
	private EnumBancos banco;
	private String agencia;
	private String numeroConta;
	private Double saldo;

	public Conta(Cliente cliente, EnumBancos banco, String agencia, String numeroConta, Double saldo) {
		super();
		this.cliente = cliente;
		this.banco = banco;
		this.agencia = agencia;
		this.numeroConta = numeroConta;
		this.saldo = saldo;
	}

	@Override
	public String toString() {
		return "Conta [cliente=" + cliente + ", banco=" + banco + ", agencia=" + agencia + ", numeroConta="
				+ numeroConta + ", saldo=" + saldo + "]";
	}
	
	public EnumBancos getBanco() {
		return banco;
	}
	public Cliente getCliente() {
		return cliente;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public String getAgencia() {
		return agencia;
	}
	
	public String getNumeroConta() {
		return numeroConta;
	}
	
	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

}