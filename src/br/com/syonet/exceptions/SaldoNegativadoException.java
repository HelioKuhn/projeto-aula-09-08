package br.com.syonet.exceptions;

public class SaldoNegativadoException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public SaldoNegativadoException(String mensagem) {
		super(mensagem);
	}

}
