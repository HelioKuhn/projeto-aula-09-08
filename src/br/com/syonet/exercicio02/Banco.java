package br.com.syonet.exercicio02;

import java.util.ArrayList;
import java.util.List;

import br.com.syonet.exceptions.ContaNaoEncontradaException;
import br.com.syonet.exceptions.SaldoInsuficienteException;
import br.com.syonet.exceptions.SaldoNegativadoException;
import br.com.syonet.exercicio01.Cliente;
import br.com.syonet.exercicio01.Conta;

public class Banco {

	private EnumBancos nomeBanco;
	private List<Conta> contas = new ArrayList<Conta>();

	public Banco(EnumBancos nomeBanco) {
		this.nomeBanco = nomeBanco;
	}

	public void addConta(Conta conta) {
		if (conta.getBanco() == nomeBanco) {
			contas.add(conta);
		}
	}

	public List<Conta> listarContas() {
		return contas;
	}

	public Conta buscarPorCPF(String cpf) {
		return contas.stream().filter(c -> c.getCliente().getCPF().equals(cpf)).findAny()
				.orElseThrow(() -> new ContaNaoEncontradaException("Conta com cpf não " + cpf + " foi encontrada!!"));
	}

	public double consultaSaldo(String cpf) {
			Conta conta = buscarPorCPF(cpf);
			if(conta.getSaldo() < 0) {
				throw new SaldoNegativadoException("Saldo negativo!!!");
			}else return conta.getSaldo();
			
	}
	
	public Double saque(String CPF, EnumBancos banco, String agencia, String numeroConta, Double valorSaque) {
		Conta conta = buscarPorCPF(CPF);
		consultaSaldo(CPF);
		if(conta.getBanco() == banco && conta.getAgencia()==agencia &&conta.getNumeroConta()==numeroConta) {
			if(conta.getSaldo()>=valorSaque) {
				conta.setSaldo(conta.getSaldo() - valorSaque);
				return conta.getSaldo();
			}else throw new SaldoInsuficienteException("Saldo Insuficiente!!!");
		}else throw new ContaNaoEncontradaException("O cliente com cpf " + CPF + " não está registrado neste Banco/Agencia");
	}
	
	

}
