package br.com.syonet.exercicio02;

import br.com.syonet.exercicio01.Cliente;
import br.com.syonet.exercicio01.Conta;

public class Main {

	public static void main(String[] args) {
		
		Cliente cliente1 = new Cliente("02402", "Hélio Kuhn");
		
		Conta conta1 = new Conta(cliente1, EnumBancos.BRADESCO, "1492","1492",3773.00);
		
		Banco banco1 = new Banco(EnumBancos.BRADESCO);
		
		banco1.addConta(conta1);
		
		//deixei comentado para validar o exercicio 3
		//double saldo = banco1.consultaSaldo("02402");
		//System.out.println("Saldo: " + saldo);
		
		
		Double saldoAposSaque = banco1.saque("02402", EnumBancos.BRADESCO, "1491", "1492", 5000.00);
		System.out.println(saldoAposSaque);
		
		
	}

}
